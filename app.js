const express = require('express')
const mongoose = require('./server/connection')
const cors = require('cors')
const bodyParser = require('body-parser')


const app = express()
const port = 5000
const router = require('./server/router/routes')

app.use(cors({ credentials: true, origin : ['http://localhost:3000', 'http://localhost:3001'] }))


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(router)

console.log('hithere')


app.listen(port,()=>{
    console.log(`starting app on ${port}`)
})