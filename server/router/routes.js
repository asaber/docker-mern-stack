const express = require('express')
const routes = express.Router()
const Todo = require('./../models/TodoModel')

routes.post('/api/addTodo', (req,res)=>{
    console.log(req.body.text)
    let newTodo = new Todo({
        text : req.body.text
    })
    newTodo.save()
    .then((todo)=>{
        console.log(`a todo was saved and is : ${todo}`)
        res.json({status : 'succeded', message : todo})

    })
    .catch((err)=> {
        console.log(`err occured during adding todo and is : ${err}`)
        res.json({status : 'failed', message : err})
    })
})



routes.get('/api/viewTodos', (req,res)=>{
    Todo.find()
    .then((todos)=>{
        console.log(todos)
        res.json({status : 'succeded', message : todos })
    })
    .catch((err)=>{
        console.log(err)
        res.json({status : 'failed', message : err })
    })
})

module.exports  = routes