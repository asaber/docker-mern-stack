const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TodoSchema = new Schema({
    type : {
        type : String,
        enum : ['urgent', 'normal'],
        default : 'normal'
    },
    text : {
        type : String,
        required : true
    },
    addedAt : {
        type : Date,
        default :Date.now()
    }

})


const Todo = mongoose.model('Todo', TodoSchema)

module.exports = Todo